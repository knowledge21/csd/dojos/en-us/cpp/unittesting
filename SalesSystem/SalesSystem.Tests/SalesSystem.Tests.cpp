#include "pch.h"
#include "CppUnitTest.h"
#include "../SalesSystem/ComissionCalculator.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SalesSystemTests
{
	TEST_CLASS(ComissionCalculatorTests)
	{
	public:
		TEST_METHOD(Test500USDSaleReturns25USDComission)
		{
			int saleValue = 500;
			int expectedComission = 25;

			ComissionCalculator calculator;

			int calculatedComission = calculator.calculate(saleValue);

			Assert::AreEqual(expectedComission, calculatedComission);

		}

		TEST_METHOD(Test10000USDSaleReturns600USDComission)
		{
			int saleValue = 10000;
			int expectedComission = 600;

			ComissionCalculator calculator;

			int calculatedComission = calculator.calculate(saleValue);

			Assert::AreEqual(expectedComission, calculatedComission);

		}
	};
}
